#
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "error:health.patient.ambulatory_care:"
msgid "No health professional associated to this user"
msgstr ""

msgctxt "error:health.patient.rounding:"
msgid "No health professional associated to this user"
msgstr ""

msgctxt "field:health.ambulatory_care_procedure,comments:"
msgid "Comments"
msgstr ""

msgctxt "field:health.ambulatory_care_procedure,create_date:"
msgid "Create Date"
msgstr ""

msgctxt "field:health.ambulatory_care_procedure,create_uid:"
msgid "Create User"
msgstr ""

msgctxt "field:health.ambulatory_care_procedure,id:"
msgid "ID"
msgstr ""

msgctxt "field:health.ambulatory_care_procedure,name:"
msgid "Session"
msgstr ""

msgctxt "field:health.ambulatory_care_procedure,procedure:"
msgid "Code"
msgstr ""

msgctxt "field:health.ambulatory_care_procedure,rec_name:"
msgid "Name"
msgstr ""

msgctxt "field:health.ambulatory_care_procedure,write_date:"
msgid "Write Date"
msgstr ""

msgctxt "field:health.ambulatory_care_procedure,write_uid:"
msgid "Write User"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,base_condition:"
msgid "Condition"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,bpm:"
msgid "Heart Rate"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,create_date:"
msgid "Create Date"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,create_uid:"
msgid "Create User"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,diastolic:"
msgid "Diastolic Pressure"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,evaluation:"
msgid "Related Evaluation"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,evolution:"
msgid "Evolution"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,glycemia:"
msgid "Glycemia"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,health_professional:"
msgid "Health Professional"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,id:"
msgid "ID"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,name:"
msgid "ID"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,next_session:"
msgid "Next Session"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,ordering_professional:"
msgid "Requested by"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,osat:"
msgid "Oxygen Saturation"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,patient:"
msgid "Patient"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,procedures:"
msgid "Procedures"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,rec_name:"
msgid "Name"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,respiratory_rate:"
msgid "Respiratory Rate"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,session_end:"
msgid "End"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,session_notes:"
msgid "Notes"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,session_number:"
msgid "Session #"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,session_start:"
msgid "Start"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,signed_by:"
msgid "Signed by"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,state:"
msgid "State"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,systolic:"
msgid "Systolic Pressure"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,temperature:"
msgid "Temperature"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,warning:"
msgid "Warning"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,warning_icon:"
msgid "Warning Icon"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,write_date:"
msgid "Write Date"
msgstr ""

msgctxt "field:health.patient.ambulatory_care,write_uid:"
msgid "Write User"
msgstr ""

msgctxt "field:health.patient.rounding,bpm:"
msgid "Heart Rate"
msgstr ""

msgctxt "field:health.patient.rounding,code:"
msgid "Code"
msgstr ""

msgctxt "field:health.patient.rounding,create_date:"
msgid "Create Date"
msgstr ""

msgctxt "field:health.patient.rounding,create_uid:"
msgid "Create User"
msgstr ""

msgctxt "field:health.patient.rounding,depression:"
msgid "Depression signs"
msgstr ""

msgctxt "field:health.patient.rounding,diastolic:"
msgid "Diastolic Pressure"
msgstr ""

msgctxt "field:health.patient.rounding,diuresis:"
msgid "Diuresis"
msgstr ""

msgctxt "field:health.patient.rounding,environmental_assessment:"
msgid "Environment"
msgstr ""

msgctxt "field:health.patient.rounding,evaluation_end:"
msgid "End"
msgstr ""

msgctxt "field:health.patient.rounding,evaluation_start:"
msgid "Start"
msgstr ""

msgctxt "field:health.patient.rounding,evolution:"
msgid "Evolution"
msgstr ""

msgctxt "field:health.patient.rounding,glycemia:"
msgid "Glycemia"
msgstr ""

msgctxt "field:health.patient.rounding,health_professional:"
msgid "Health Professional"
msgstr ""

msgctxt "field:health.patient.rounding,id:"
msgid "ID"
msgstr ""

msgctxt "field:health.patient.rounding,name:"
msgid "Registration Code"
msgstr ""

msgctxt "field:health.patient.rounding,osat:"
msgid "Oxygen Saturation"
msgstr ""

msgctxt "field:health.patient.rounding,pain:"
msgid "Pain"
msgstr ""

msgctxt "field:health.patient.rounding,pain_level:"
msgid "Pain"
msgstr ""

msgctxt "field:health.patient.rounding,personal_needs:"
msgid "Personal needs"
msgstr ""

msgctxt "field:health.patient.rounding,position:"
msgid "Position"
msgstr ""

msgctxt "field:health.patient.rounding,potty:"
msgid "Potty"
msgstr ""

msgctxt "field:health.patient.rounding,procedures:"
msgid "Procedures"
msgstr ""

msgctxt "field:health.patient.rounding,proximity:"
msgid "Proximity"
msgstr ""

msgctxt "field:health.patient.rounding,pump:"
msgid "Pumps"
msgstr ""

msgctxt "field:health.patient.rounding,rec_name:"
msgid "Name"
msgstr ""

msgctxt "field:health.patient.rounding,report_end_date:"
msgid "End Date"
msgstr ""

msgctxt "field:health.patient.rounding,report_end_time:"
msgid "End Time"
msgstr ""

msgctxt "field:health.patient.rounding,report_start_date:"
msgid "Start Date"
msgstr ""

msgctxt "field:health.patient.rounding,report_start_time:"
msgid "Start Time"
msgstr ""

msgctxt "field:health.patient.rounding,respiratory_rate:"
msgid "Respiratory Rate"
msgstr ""

msgctxt "field:health.patient.rounding,round_summary:"
msgid "Round Summary"
msgstr ""

msgctxt "field:health.patient.rounding,signed_by:"
msgid "Signed by"
msgstr ""

msgctxt "field:health.patient.rounding,state:"
msgid "State"
msgstr ""

msgctxt "field:health.patient.rounding,systolic:"
msgid "Systolic Pressure"
msgstr ""

msgctxt "field:health.patient.rounding,temperature:"
msgid "Temperature"
msgstr ""

msgctxt "field:health.patient.rounding,urinary_catheter:"
msgid "Urinary Catheter"
msgstr ""

msgctxt "field:health.patient.rounding,warning:"
msgid "Warning"
msgstr ""

msgctxt "field:health.patient.rounding,warning_icon:"
msgid "Warning Icon"
msgstr ""

msgctxt "field:health.patient.rounding,weight:"
msgid "Weight"
msgstr ""

msgctxt "field:health.patient.rounding,write_date:"
msgid "Write Date"
msgstr ""

msgctxt "field:health.patient.rounding,write_uid:"
msgid "Write User"
msgstr ""

msgctxt "field:health.rounding_procedure,create_date:"
msgid "Create Date"
msgstr ""

msgctxt "field:health.rounding_procedure,create_uid:"
msgid "Create User"
msgstr ""

msgctxt "field:health.rounding_procedure,id:"
msgid "ID"
msgstr ""

msgctxt "field:health.rounding_procedure,name:"
msgid "Rounding"
msgstr ""

msgctxt "field:health.rounding_procedure,notes:"
msgid "Notes"
msgstr ""

msgctxt "field:health.rounding_procedure,procedure:"
msgid "Code"
msgstr ""

msgctxt "field:health.rounding_procedure,rec_name:"
msgid "Name"
msgstr ""

msgctxt "field:health.rounding_procedure,write_date:"
msgid "Write Date"
msgstr ""

msgctxt "field:health.rounding_procedure,write_uid:"
msgid "Write User"
msgstr ""

msgctxt "field:health.sequences,ambulatory_care_sequence:"
msgid "Health Ambulatory Care"
msgstr ""

msgctxt "field:health.sequences,patient_rounding_sequence:"
msgid "Health Rounding"
msgstr ""

msgctxt "help:health.ambulatory_care_procedure,procedure:"
msgid "Procedure Code, for example ICD-10-PCS Code 7-character string"
msgstr ""

msgctxt "help:health.patient.ambulatory_care,bpm:"
msgid "Heart rate expressed in beats per minute"
msgstr ""

msgctxt "help:health.patient.ambulatory_care,evolution:"
msgid "Check your judgement of current patient condition"
msgstr ""

msgctxt "help:health.patient.ambulatory_care,glycemia:"
msgid "Blood Glucose level"
msgstr ""

msgctxt "help:health.patient.ambulatory_care,osat:"
msgid "Oxygen Saturation(arterial)."
msgstr ""

msgctxt "help:health.patient.ambulatory_care,procedures:"
msgid ""
"List of the procedures in this session. Please enter the first one as the "
"main procedure"
msgstr ""

msgctxt "help:health.patient.ambulatory_care,respiratory_rate:"
msgid "Respiratory rate expressed in breaths per minute"
msgstr ""

msgctxt "help:health.patient.ambulatory_care,signed_by:"
msgid "Health Professional that signed the session"
msgstr ""

msgctxt "help:health.patient.ambulatory_care,temperature:"
msgid "Temperature in celsius"
msgstr ""

msgctxt "help:health.patient.ambulatory_care,warning:"
msgid ""
"Check this box to alert the supervisor about this session. A warning icon "
"will be shown in the session list"
msgstr ""

msgctxt "help:health.patient.rounding,bpm:"
msgid "Heart rate expressed in beats per minute"
msgstr ""

msgctxt "help:health.patient.rounding,depression:"
msgid "Check this if the patient shows signs of depression"
msgstr ""

msgctxt "help:health.patient.rounding,diuresis:"
msgid "volume in ml"
msgstr ""

msgctxt "help:health.patient.rounding,environmental_assessment:"
msgid "Environment assessment . State any disorder in the room."
msgstr ""

msgctxt "help:health.patient.rounding,evolution:"
msgid "Check your judgement of current patient condition"
msgstr ""

msgctxt "help:health.patient.rounding,glycemia:"
msgid "Blood Glucose level"
msgstr ""

msgctxt "help:health.patient.rounding,osat:"
msgid "Oxygen Saturation(arterial)."
msgstr ""

msgctxt "help:health.patient.rounding,pain:"
msgid "Check if the patient is in pain"
msgstr ""

msgctxt "help:health.patient.rounding,pain_level:"
msgid "Enter the pain level, from 1 to 10"
msgstr ""

msgctxt "help:health.patient.rounding,personal_needs:"
msgid "Check if the patient requests anything"
msgstr ""

msgctxt "help:health.patient.rounding,position:"
msgid "Check if the patient needs to be repositioned or is unconfortable"
msgstr ""

msgctxt "help:health.patient.rounding,potty:"
msgid "Check if the patient needs to urinate / defecate"
msgstr ""

msgctxt "help:health.patient.rounding,procedures:"
msgid ""
"List of the procedures in this rounding. Please enter the first one as the "
"main procedure"
msgstr ""

msgctxt "help:health.patient.rounding,proximity:"
msgid "Check if personal items, water, alarm, ... are not in easy reach"
msgstr ""

msgctxt "help:health.patient.rounding,pump:"
msgid "Check if there is any issues with the pumps - IVs ... "
msgstr ""

msgctxt "help:health.patient.rounding,respiratory_rate:"
msgid "Respiratory rate expressed in breaths per minute"
msgstr ""

msgctxt "help:health.patient.rounding,signed_by:"
msgid "Health Professional that signed the rounding"
msgstr ""

msgctxt "help:health.patient.rounding,temperature:"
msgid "Temperature in celsius"
msgstr ""

msgctxt "help:health.patient.rounding,warning:"
msgid ""
"Check this box to alert the supervisor about this patient rounding. A "
"warning icon will be shown in the rounding list"
msgstr ""

msgctxt "help:health.patient.rounding,weight:"
msgid "Measured weight, in kg"
msgstr ""

msgctxt "help:health.rounding_procedure,procedure:"
msgid "Procedure Code, for example ICD-10-PCS Code 7-character string"
msgstr ""

msgctxt "model:health.ambulatory_care_procedure,name:"
msgid "Ambulatory Care Procedure"
msgstr ""

msgctxt "model:health.patient.ambulatory_care,name:"
msgid "Patient Ambulatory Care"
msgstr ""

msgctxt "model:health.patient.rounding,name:"
msgid "Patient Rounding"
msgstr ""

msgctxt "model:health.rounding_procedure,name:"
msgid "Rounding - Procedure"
msgstr ""

msgctxt "model:ir.action,name:action_health_patient_ambulatory_care_view"
msgid "Ambulatory Care"
msgstr ""

msgctxt "model:ir.action,name:action_health_patient_rounding_view"
msgid "Roundings"
msgstr ""

msgctxt "model:ir.action,name:report_round_nursing"
msgid "Nursing rounds"
msgstr ""

msgctxt "model:ir.sequence,name:seq_health_ambulatory_care"
msgid "Ambulatory Care"
msgstr ""

msgctxt "model:ir.sequence,name:seq_health_patient_rounding"
msgid "Patient Rounding"
msgstr ""

msgctxt "model:ir.sequence.type,name:seq_type_health_ambulatory_care"
msgid "Ambulatory Care"
msgstr ""

msgctxt "model:ir.sequence.type,name:seq_type_health_patient_rounding"
msgid "Patient Rounding"
msgstr ""

msgctxt "model:ir.ui.menu,name:health_nursing_menu"
msgid "Nursing"
msgstr ""

msgctxt "model:ir.ui.menu,name:menu_health_patient_ambulatory_care_list"
msgid "Ambulatory Care"
msgstr ""

msgctxt "model:ir.ui.menu,name:menu_health_patient_rounding_list"
msgid "Roundings"
msgstr ""

msgctxt "model:res.group,name:group_health_nursing"
msgid "Health Nursing"
msgstr ""

msgctxt "model:res.group,name:group_health_nursing_admin"
msgid "Health Nursing Administration"
msgstr ""

msgctxt "model:res.user,name:user_demo_nurse"
msgid "Health Nurse"
msgstr ""

msgctxt "selection:health.patient.ambulatory_care,evolution:"
msgid ""
msgstr ""

msgctxt "selection:health.patient.ambulatory_care,evolution:"
msgid "Improving"
msgstr ""

msgctxt "selection:health.patient.ambulatory_care,evolution:"
msgid "Initial"
msgstr ""

msgctxt "selection:health.patient.ambulatory_care,evolution:"
msgid "Status Quo"
msgstr ""

msgctxt "selection:health.patient.ambulatory_care,evolution:"
msgid "Worsening"
msgstr ""

msgctxt "selection:health.patient.ambulatory_care,state:"
msgid ""
msgstr ""

msgctxt "selection:health.patient.ambulatory_care,state:"
msgid "Done"
msgstr ""

msgctxt "selection:health.patient.ambulatory_care,state:"
msgid "In Progress"
msgstr ""

msgctxt "selection:health.patient.rounding,evolution:"
msgid ""
msgstr ""

msgctxt "selection:health.patient.rounding,evolution:"
msgid "Improving"
msgstr ""

msgctxt "selection:health.patient.rounding,evolution:"
msgid "Status Quo"
msgstr ""

msgctxt "selection:health.patient.rounding,evolution:"
msgid "Worsening"
msgstr ""

msgctxt "selection:health.patient.rounding,state:"
msgid ""
msgstr ""

msgctxt "selection:health.patient.rounding,state:"
msgid "Done"
msgstr ""

msgctxt "selection:health.patient.rounding,state:"
msgid "In Progress"
msgstr ""
